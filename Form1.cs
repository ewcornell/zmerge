﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void cmdZMerge_Click(object sender, EventArgs e)
        {
            mergeZ();
        }

        public void mergeZ()
        {
            OpenFileDialog.InitialDirectory = @"c:\data\3D";
            OpenFileDialog.Filter = "Prism Files(*.zpri)|*.zpri";

            if (OpenFileDialog.ShowDialog() == DialogResult.OK)
            {
                string[] names = OpenFileDialog.FileNames;
                FileStream[] fs = new FileStream[names.Length];
                FileStream[] bs = new FileStream[names.Length];

                FileStream fw, bw, fws, bws;
                if (names.Length == 1)
                {
                    fw = File.OpenWrite(names[0].Replace(".z", "-2."));
                    bw = File.OpenWrite(names[0].Replace(".z", "-2.") + ".bri");
                    fws = File.OpenWrite(names[0].Replace(".z", "-s."));
                    bws = File.OpenWrite(names[0].Replace(".z", "-s.") + ".bri");
                }
                else
                {
                    fw = File.OpenWrite(names[0].Replace(".pri", "-2.pri"));
                    bw = File.OpenWrite(names[0].Replace(".pri", "-2.pri") + ".bri");
                    fws = File.OpenWrite(names[0].Replace(".pri", "-s.pri"));
                    bws = File.OpenWrite(names[0].Replace(".pri", "-s.pri") + ".bri");
                }

                float[] head = { 1, 2, 3 }, head2 = { 1, 2, 3 };
                for (int i1 = 0; i1 < names.Length; i1++)
                {
                    fs[i1] = File.OpenRead(names[i1]);
                    bs[i1] = File.OpenRead(names[i1] + ".bri");
                    head = getFloat2(10, fs[i1]);
                    head2 = getFloat2(10, bs[i1]);
                }

                int hgt = (int)head[5];
                int wid = (int)head[2];
                int npass = (int)head[7];
                int nz = (int)head[9];
                float dz = head[8] * 1000;
                if (names.Length > 1)
                {
                    nz = names.Length;
                    dz = 150;
                }

                // compute wid from file size in case run was aborted in middle
                FileInfo fi = new FileInfo(names[0]);
                int wid2 = (int)Math.Floor((double)fi.Length / 4 / hgt / nz / 180);
                if (names.Length > 1)
                    wid2 = (int)Math.Floor((double)fi.Length / 4 / hgt / 180);
                Console.WriteLine("wid size2: " + wid2);
                //wid2 = 2;

                head2[2] = wid2;
                head2[9] = 0;
                putFloat2(head2, fw);
                putFloat2(head2, bw);
                putFloat2(head2, fws);
                putFloat2(head2, bws);
                int nn = 180 * hgt;
                float[] dt2 = new float[nn], br2 = new float[nn];
                float[] dts = new float[nn], brs = new float[nn];
                float lstB = 100, lstD = 200, smd, smb, smn, smz;

                stat.Text = "z merge started";

                pb1.Maximum = wid2;
                for (int i2 = 0; i2 < wid2; i2++)
                {
                    Application.DoEvents();
                    pb1.Value = i2;
                    float[][] dt = new float[nz][];
                    float[][] br = new float[nz][];
                    for (int i = 0; i < nz; i++)
                    {
                        if (names.Length > 1)  // ----- get data from several files ------
                        {
                            dt[i] = getFloat2(nn, fs[i]);    
                            br[i] = getFloat2(nn, bs[i]);
                        }
                        else    // ------ get data from one file with Z-stack ------
                        {
                            dt[i] = getFloat2(nn, fs[0]);
                            br[i] = getFloat2(nn, bs[0]);
                        }
                    }
                    //float[] dt = getFloat2(nn * nz, fs);
                    // float[] br = getFloat2(nn * nz, bs);
                    Bitmap bmp = new Bitmap(hgt / 10, 180, System.Drawing.Imaging.PixelFormat.Format24bppRgb);
                    for (int i = 0; i < 180; i++)
                    {
                        Application.DoEvents();
                        float[,] hz = new float[2, nz];
                        float[,] hzz = new float[4, hgt];
                        for (int j = 0; j < hgt; j++)
                        {
                            int ii = j * 180 + i;
                            smb = smd = smn = smz = 0;
                            for (int z = 0; z < nz; z++)
                            {
                                // ------- see which layers in the Z stack have data which isn't too dark and which is the range of the probe -------
                                if (br[z][ii] > 150 && dt[z][ii] < 495 && dt[z][ii] > 5)  
                                {
                                    smb += br[z][ii];              // ---- to calculate average brightness of good passes ----
                                    smd += dt[z][ii] + dz * z;     // ---- to calculate average Z height of good passes ----
                                    smz += z;                      // ---- to caculate average Z index of good passes ----
                                    smn++;
                                }
                                hz[0, z] = dt[z][ii];
                                hz[1, z] = br[z][ii];
                            }

                            if (smn == 0)      // ------- if none of the passes had good data use last good values -------
                            {
                                br2[ii] = lstB;
                                dt2[ii] = lstD;
                                hzz[0, j] = lstB;
                                hzz[1, j] = lstD;
                                hzz[2, j] = -1;
                            }
                            else   // ---- compute average values ----
                            {
                                br2[ii] = lstB = smb / smn;
                                dt2[ii] = lstD = smd / smn;
                                hzz[0, j] = lstB;
                                hzz[1, j] = lstD;
                                hzz[2, j] = smz / smn;
                            }
                            br2[ii] = br[0][ii];
                            dt2[ii] = dt[0][ii];

                            if (j % 10 == 0) // ----- update the bitmap -----
                            {
                                int c = (int)(smz * 20 / (smn + 1));
                                bmp.SetPixel(j/10, i, Color.FromArgb(c, c, c));
                                pictureBox1.Image = (Image)bmp;
                            }
                        }

                        // ---- smooth ----
                        for (int j = 0; j < hgt; j++)
                        {
                            smn = smz = 0;
                            for (int i5 = -10; i5 < 10; i5++)
                            {
                                int i6 = i5 + j;
                                float wght = 20 - Math.Abs(i5);
                                if (i6 >= 0 && i6 < hgt && hzz[2, i6] >= 0)
                                {
                                    smn += wght;
                                    smz += hzz[2, i6] * wght;
                                }
                            }
                            int ii = j * 180 + i;
                            if (smn > 0)
                            {
                                hzz[3, j] = smz / smn;
                                int iz1 = (int)Math.Floor(hzz[3, j]);
                                int iz2 = iz1 + 1;
                                if (iz2 == nz) iz2 = iz1;
                                float sc2 = hzz[3, j] - iz1;
                                float sc1 = 1 - sc2;
                                dts[ii] = sc1 * dt[iz1][ii] + sc2 * (dt[iz2][ii] + dz) + dz * iz1;
                                brs[ii] = sc1 * br[iz1][ii] + sc2 * br[iz2][ii];
                            }
                            else
                            {
                                hzz[3, j] = -1;
                                brs[ii] = br2[ii];
                                dts[ii] = dt2[ii];
                            }
                        }
                    }
                    putFloat2(br2, bw);
                    putFloat2(dt2, fw);
                    putFloat2(brs, bws);
                    putFloat2(dts, fws);
                }

                for (int i = 0; i < names.Length; i++)
                {
                    fs[i].Close();
                    bs[i].Close();
                }
                fw.Close();
                bw.Close();
                stat.Text = "z merge done";
            } // openDlg
        }  // mergeZ()

        public float[] getFloat2(int n, FileStream fs)
        {
            float[] f = new float[n];
            byte[] b = new byte[n * 4];
            fs.Read(b, 0, n * 4);
            //temporary variable
            byte bt = 0;
            for (int i = 0; i < 4 * n; i += 4)
            {
                bt = b[i + 1]; b[i + 1] = b[i + 2]; b[i + 2] = bt;
                bt = b[i + 0]; b[i + 0] = b[i + 3]; b[i + 3] = bt;
            }
            MemoryStream ms = new MemoryStream(b);
            BinaryReader br = new BinaryReader(ms);
            for (int i = 0; i < n; i++)
            {
                f[i] = br.ReadSingle();
                if (f[i] > 450 || f[i] < 0)
                {
                    byte[] bs = { b[i * 4 + 0], b[i * 4 + 1], b[i * 4 + 2], b[i * 4 + 3] };
                    float ff = f[i];
                }
            }

            return f;
        }
        public void putFloat2(float[] f, FileStream fs)
        {
            byte[] b = new byte[f.Length * 4];
            for (int i = 0; i < f.Length; i++)
            {
                byte[] b2 = BitConverter.GetBytes(f[i]);
                Array.Reverse(b2);
                for (int i1 = 0; i1 < 4; i1++)
                    fs.WriteByte(b2[i1]);
            }
        }
    }
}
